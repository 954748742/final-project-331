package se331.lab.rest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import se331.lab.rest.entity.*;
import se331.lab.rest.repository.*;
import se331.lab.rest.security.entity.Authority;
import se331.lab.rest.security.entity.AuthorityName;
import se331.lab.rest.security.entity.User;
import se331.lab.rest.security.repository.AuthorityRepository;
import se331.lab.rest.security.repository.UserRepository;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@Component
public class DataLoader implements ApplicationRunner {
  @Autowired
  StudentRepository studentRepository;
  @Autowired
  TeacherRepository teacherRepository;
  @Autowired
  ActivityRepository activityRepository;
  @Autowired
  AuthorityRepository authorityRepository;
  @Autowired
  UserRepository userRepository;
  @Autowired
  EnrolsRepository enrolsRepository;
  @Autowired
  AdminRepository adminRepository;
  @Autowired
  CommentRepository commentRepository;

  @Override
  @Transactional
  public void run(ApplicationArguments args) throws Exception {
    Student student1 = Student.builder()
      .stid("602115512")
      .name("Siyang")
      .surname("Huang")
      .imgurl("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1576077987&di=7c37979b8382152cffe38cf5bdf07655&imgtype=jpg&er=1&src=http%3A%2F%2Fwww.5jjc.net%2Ftu5jaHR0cHM6Ly9pbWcuYWxpY2RuLmNvbS90ZnNjb20vaTIvVEIxZ3B5ZUhwJDZheWFwJDUkMw.jpg")
      .dob("1998-01-26")
      .build();
    Student student2 = Student.builder()
      .stid("602115505")
      .name("Dongze")
      .surname("Li")
      .imgurl("https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=273838610,213832631&fm=26&gp=0.jpg")
      .dob("1997-07-16")
      .build();
    Student student3 = Student.builder()
      .stid("602115555")
      .name("Auto")
      .surname("Man")
      .imgurl("https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=3028495485,2938166697&fm=26&gp=0.jpg")
      .dob("2001-01-01")
      .build();
    Student student4 = Student.builder()
      .stid("602115566")
      .name("naruto")
      .surname("Wuzimaki")
      .imgurl("https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3678937778,506723037&fm=26&gp=0.jpg")
      .dob("1999-04-23")
      .build();
    Student student5 = Student.builder()
      .stid("602115577")
      .name("Capatin")
      .surname("American")
      .imgurl("https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2388006436,2769696963&fm=26&gp=0.jpg")
      .dob("1999-04-23")
      .build();
    Student student6 = Student.builder()
      .stid("602115588")
      .name("nezha")
      .surname("Li")
      .imgurl("https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=1866728759,3586786375&fm=26&gp=0.jpg")
      .dob("1997-12-26")
      .build();

    this.studentRepository.save(student1);
    this.studentRepository.save(student2);
    this.studentRepository.save(student3);
    this.studentRepository.save(student4);
    this.studentRepository.save(student5);
    this.studentRepository.save(student6);

    Teacher teacher1 = Teacher.builder()
      .name("Jackson")
      .surname("Wang")
      .imgurl("https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=1298102972,3392023132&fm=26&gp=0.jpg")
      .build();
    Teacher teacher2 = Teacher.builder()
      .name("Rose")
      .surname("Luo")
      .imgurl("https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=2410384168,1025441764&fm=26&gp=0.jpg")
      .build();
    Teacher teacher3 = Teacher.builder()
      .name("Ying")
      .surname("Yang")
      .imgurl("https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=418728880,692879370&fm=26&gp=0.jpg")
      .build();
    Teacher teacher4 = Teacher.builder()
      .name("Bo")
      .surname("Huang")
      .imgurl("https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=3740237276,3222823580&fm=26&gp=0.jpg")
      .build();
    this.teacherRepository.save(teacher1);
    this.teacherRepository.save(teacher2);
    this.teacherRepository.save(teacher3);
    this.teacherRepository.save(teacher4);
//    List<String> comment1 = new ArrayList<>();
//    List<String> comment2 = new ArrayList<>();
//    List<String> comment3 = new ArrayList<>();
//    List<String> comment4 = new ArrayList<>();
//    List<String> comment5 = new ArrayList<>();
    Comment c1 = Comment.builder()
      .msg("Nice!!!")
      .time("08/02/2019")
      .activity(1)
      .avatar(teacher1.getImgurl())
      .imgurl("https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1491122825,2026009930&fm=26&gp=0.jpg")
      .name("Jackson")
      .build();
    Comment c2 = Comment.builder()
      .msg("Awesome!!!")
      .time("09/02/2019")
      .activity(1)
      .avatar(student2.getImgurl())
      .imgurl("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1576079654&di=e7659a25a9a541ba77b8dd0b74732606&imgtype=jpg&er=1&src=http%3A%2F%2Fimg.99danji.com%2Fuploadfile%2F2017%2F0721%2F20170721015025384.jpg")
      .name("Dongze")
      .build();
    Comment c3 = Comment.builder()
      .msg("Dope!!!")
      .time("19/02/2019")
      .activity(2)
      .avatar(student1.getImgurl())
      .imgurl("https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3518916885,1593341747&fm=26&gp=0.jpg")
      .name("Siyang")
      .build();
    Comment c4 = Comment.builder()
      .msg("Nope!!!")
      .time("18/02/2018")
      .activity(2)
      .avatar(teacher3.getImgurl())
      .imgurl("https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=2101741964,3770227362&fm=26&gp=0.jpg")
      .name("Ying")
      .build();
    Comment c5 = Comment.builder()
      .msg("So sad!!!")
      .time("15/02/2018")
      .activity(3)
      .avatar(teacher4.getImgurl())
      .imgurl("https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2010357963,391853494&fm=26&gp=0.jpg")
      .name("Bo")
      .build();
    Comment c6 = Comment.builder()
      .msg("Seriously?")
      .time("22/02/2018")
      .activity(4)
      .avatar(student4.getImgurl())
      .imgurl("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1576079908&di=d4007a8e45846bc643ce1e37b581a9ac&imgtype=jpg&er=1&src=http%3A%2F%2Fn.sinaimg.cn%2Fsinacn13%2F40%2Fw480h360%2F20180325%2Fe5fe-fysnevm9405914.jpg")
      .name("naruto")
      .build();

    this.commentRepository.save(c1);
    this.commentRepository.save(c2);
    this.commentRepository.save(c3);
    this.commentRepository.save(c4);
    this.commentRepository.save(c5);
    this.commentRepository.save(c6);


    Activity activity1 = Activity.builder()
      .desc("We will have fun")
      .loc("ChiangMai venues")
      .name("Motorcycle race")
      .when("2019-10-31")
      .time("12:00")
      .regStart("2019-10-14")
      .regEnd("2019-10-29")
      .build();
    Activity activity2 = Activity.builder()
      .desc("We will see which is the best car")
      .loc("121Street")
      .name("Drag race")
      .when("2019-11-30")
      .time("12:00")
      .regStart("2019-11-14")
      .regEnd("2019-11-29")
      .build();
    Activity activity3 = Activity.builder()
      .desc("Feel stimulation")
      .loc("CMU")
      .name("Cosplay of Counter Strike")
      .when("2019-12-31")
      .time("12:00")
      .regStart("2019-12-14")
      .regEnd("2019-12-29")
      .build();
    Activity activity4 = Activity.builder()
      .desc("Realize the dream of otaku")
      .loc("CAMT")
      .name("Cosplay")
      .when("2019-10-27")
      .time("09:00")
      .regStart("2019-08-10")
      .regEnd("2019-10-27")
      .build();
    Activity activity5 = Activity.builder()
      .desc("The bodybuilders flexed their muscles")
      .loc("language institution")
      .name("Fitness show")
      .when("2019-11-03")
      .time("06:00")
      .regStart("2018-08-26")
      .regEnd("2019-11-02")
      .build();
    this.activityRepository.save(activity1);
    this.activityRepository.save(activity2);
    this.activityRepository.save(activity3);
    this.activityRepository.save(activity4);
    this.activityRepository.save(activity5);
    Enrols enrols1 = Enrols.builder()
      .activity(2)
      .student(1)
      .status("confirmed")
      .build();
    Enrols enrols2 = Enrols.builder()
      .activity(1)
      .student(2)
      .status("confirmed")
      .build();
    Enrols enrols3 = Enrols.builder()
      .activity(3)
      .student(1)
      .status("rejected")
      .build();
    Enrols enrols4 = Enrols.builder()
      .activity(3)
      .student(3)
      .status("pending")
      .build();
    Enrols enrols5 = Enrols.builder()
      .activity(4)
      .student(3)
      .status("confirmed")
      .build();
    this.enrolsRepository.save(enrols1);
    this.enrolsRepository.save(enrols2);
    this.enrolsRepository.save(enrols3);
    this.enrolsRepository.save(enrols4);
    this.enrolsRepository.save(enrols5);
    activity1.setLecturer(teacher1);
    teacher1.getActivities().add(activity1);
    activity2.setLecturer(teacher2);
    teacher2.getActivities().add(activity2);
    activity3.setLecturer(teacher2);
    teacher2.getActivities().add(activity3);
    activity4.setLecturer(teacher4);
    teacher4.getActivities().add(activity4);
    activity5.setLecturer(teacher3);
    teacher3.getActivities().add(activity5);

    Admin admin = Admin.builder()
      .name("admin")
      .surname("admin")
      .imgurl("https://drive.google.com/uc?id=1W9VLWi53eG8y6pL25Yo68A6ig9C5F32a")
      .build();
    adminRepository.save(admin);
    PasswordEncoder encoder = new BCryptPasswordEncoder();
    Authority auth1 = Authority.builder().name(AuthorityName.ROLE_ADMIN).build();
    Authority auth2 = Authority.builder().name(AuthorityName.ROLE_TEACHER).build();
    Authority auth3 = Authority.builder().name(AuthorityName.ROLE_STUDENT).build();
    User user1, user2, user3, user4, user5, user6, user7, user8, user9, user10, user11;
    user1 = User.builder()
      .pass(encoder.encode("admin"))
      .email("admin@test.com")
      .enabled(true)
      .status("ok")
      .type("admin")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user2 = User.builder()
      .pass(encoder.encode("jackson"))
      .email("jackson@test.com")
      .enabled(true)
      .status("ok")
      .type("teacher")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user3 = User.builder()
      .pass(encoder.encode("rose"))
      .email("rose@test.com")
      .enabled(true)
      .status("ok")
      .type("teacher")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user4 = User.builder()
      .pass(encoder.encode("ying"))
      .email("ying@test.com")
      .enabled(true)
      .status("ok")
      .type("teacher")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user5 = User.builder()
      .pass(encoder.encode("bo"))
      .email("bo@test.com")
      .enabled(true)
      .status("ok")
      .type("teacher")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user6 = User.builder()
      .pass(encoder.encode("hades"))
      .email("hades@test.com")
      .enabled(true)
      .status("ok")
      .type("student")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user7 = User.builder()
      .pass(encoder.encode("howard"))
      .email("howard@test.com")
      .enabled(true)
      .status("ok")
      .type("student")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user8 = User.builder()
      .pass(encoder.encode("yifan"))
      .email("yifan@test.com")
      .enabled(true)
      .status("ok")
      .type("student")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user9 = User.builder()
      .pass(encoder.encode("kun"))
      .email("kun@test.com")
      .enabled(true)
      .status("new")
      .type("new-student")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user10 = User.builder()
      .pass(encoder.encode("gg"))
      .email("gg@test.com")
      .enabled(false)
      .status("reject")
      .type("new-student")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user11 = User.builder()
      .pass(encoder.encode("saksit"))
      .email("saksit@test.com")
      .enabled(false)
      .status("reject")
      .type("new-student")
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();

    authorityRepository.save(auth1);
    authorityRepository.save(auth2);
    authorityRepository.save(auth3);
    user1.getAuthorities().add(auth1);
    user1.getAuthorities().add(auth2);
    user1.getAuthorities().add(auth3);

    user2.getAuthorities().add(auth2);
    user2.getAuthorities().add(auth3);

    user3.getAuthorities().add(auth2);
    user3.getAuthorities().add(auth3);

    user4.getAuthorities().add(auth2);
    user4.getAuthorities().add(auth3);

    user5.getAuthorities().add(auth2);
    user5.getAuthorities().add(auth3);

    user6.getAuthorities().add(auth3);

    user7.getAuthorities().add(auth3);

    user8.getAuthorities().add(auth3);

    user9.getAuthorities().add(auth3);

    user10.getAuthorities().add(auth3);

    user11.getAuthorities().add(auth3);
    userRepository.save(user6);
    userRepository.save(user7);
    userRepository.save(user8);
    userRepository.save(user9);
    userRepository.save(user10);
    userRepository.save(user11);
    userRepository.save(user2);
    userRepository.save(user3);
    userRepository.save(user4);
    userRepository.save(user5);
    userRepository.save(user1);


    admin.setUser(user1);
    user1.setAppUser(admin);

    teacher1.setUser(user2);
    user2.setAppUser(teacher1);

    teacher2.setUser(user3);
    user3.setAppUser(teacher2);

    teacher3.setUser(user4);
    user4.setAppUser(teacher3);

    teacher4.setUser(user5);
    user5.setAppUser(teacher4);

    student1.setUser(user6);
    user6.setAppUser(student1);

    student2.setUser(user7);
    user7.setAppUser(student2);

    student3.setUser(user8);
    user8.setAppUser(student3);

    student4.setUser(user9);
    user9.setAppUser(student4);

    student5.setUser(user10);
    user10.setAppUser(student5);

    student6.setUser(user11);
    user11.setAppUser(student6);

  }


}
