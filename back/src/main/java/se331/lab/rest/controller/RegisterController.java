package se331.lab.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.Student;
import se331.lab.rest.entity.dto.StudentUserDTO;
import se331.lab.rest.entity.dto.UserDTO;
import se331.lab.rest.security.entity.Authority;
import se331.lab.rest.security.entity.AuthorityName;
import se331.lab.rest.security.entity.User;
import se331.lab.rest.security.repository.AuthorityRepository;
import se331.lab.rest.security.repository.UserRepository;
import se331.lab.rest.security.service.UserService;
import se331.lab.rest.service.StudentService;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class RegisterController {
  @Autowired
  UserService userService;
  @Autowired
  StudentService studentService;
  @Autowired
  UserRepository userRepository;
  PasswordEncoder encoder = new BCryptPasswordEncoder();
  @Autowired
  AuthorityRepository authorityRepository;
  @CrossOrigin
  @PostMapping("/reg")
  public ResponseEntity<?> saveUser(@RequestBody StudentUserDTO studentUserDTO) {
    Authority auth3 = authorityRepository.findByName(AuthorityName.ROLE_STUDENT);;
    List<Authority> authorities = new ArrayList<>();
    authorities.add(auth3);
    User user = User.builder()
      .pass(encoder.encode(studentUserDTO.getPass()))
      .email(studentUserDTO.getEmail())
      .enabled(true)
      .status(studentUserDTO.getStatus())
      .type(studentUserDTO.getType())
      .authorities(authorities)
      .lastPasswordResetDate(Date.from(LocalDate.of(2016, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
      .build();
    user = userService.save(user);
    Student student = Student.builder()
      .id(user.getId())
      .stid(studentUserDTO.getStid())
      .name(studentUserDTO.getName())
      .surname(studentUserDTO.getSurname())
      .imgurl(studentUserDTO.getImgurl())
      .dob(studentUserDTO.getDob())
      .user(user)
      .build();
    user.setAppUser(student);
    student =this.studentService.saveStudent(student);
    user = userService.save(user);
    return ResponseEntity.ok(StudentUserDTO.builder()
            .id(user.getId())
            .email(user.getEmail())
            .type(user.getType())
            .status(user.getStatus())
            .stid(student.getStid())
      .name(student.getName())
      .surname(student.getSurname())
    .imgurl(student.getImgurl())
    .dob(student.getDob())
      .build());
  }
}
