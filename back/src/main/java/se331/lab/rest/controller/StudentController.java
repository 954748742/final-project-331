package se331.lab.rest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import se331.lab.rest.entity.Student;
import se331.lab.rest.entity.dto.StudentDTO;
import se331.lab.rest.security.entity.User;
import se331.lab.rest.security.repository.UserRepository;
import se331.lab.rest.service.StudentService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@Slf4j
public class StudentController {

    @Autowired
    StudentService studentService;
    @Autowired
    UserRepository userRepository;
    @CrossOrigin
    @GetMapping("/students")
    public ResponseEntity getAllStudents() {
        log.info("the controller is call");
        return ResponseEntity.ok(this.studentService.getStudents());
    }
    @CrossOrigin
    @GetMapping("/students/{id}")
    public ResponseEntity getStudentById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(this.studentService.getStudent(id));
    }

    @PostMapping("/students")
    public ResponseEntity saveStudent(@RequestBody StudentDTO studentDTO) {
        return ResponseEntity.ok(this.studentService.saveStudent(studentDTO.getStudent()));
    }

    @PutMapping("/students/{id}")
    public ResponseEntity saveStudentByPut(@PathVariable long id,@RequestBody StudentDTO studentDTO) {
      studentDTO.setId(id);
      return ResponseEntity.ok(this.studentService.saveStudent(studentDTO.getStudent()));
    }

}
