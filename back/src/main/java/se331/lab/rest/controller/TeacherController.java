package se331.lab.rest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import se331.lab.rest.entity.dto.TeacherDTO;
import se331.lab.rest.service.TeacherService;

@Controller
@Slf4j
public class TeacherController {
    @Autowired
    TeacherService teacherService;
  @CrossOrigin
    @GetMapping("/teachers")
    public ResponseEntity getAllTeachers() {
        log.info("the controller is call");
        return ResponseEntity.ok(this.teacherService.getTeachers());
    }
  @CrossOrigin
  @PostMapping("/teachers")
  public ResponseEntity saveLecturer(@RequestBody TeacherDTO teacherDTO) {
    return ResponseEntity.ok(this.teacherService.saveTeacher(teacherDTO.getTeacher()));
  }
  @CrossOrigin
  @PutMapping("/teachers/{id}")
  public ResponseEntity saveLecturerByPut(@PathVariable long id,@RequestBody TeacherDTO teacherDTO) {
    teacherDTO.setId(id);
    return ResponseEntity.ok(this.teacherService.saveTeacher(teacherDTO.getTeacher()));
  }
}
