package se331.lab.rest.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Comment;
import se331.lab.rest.repository.CommentRepository;

import java.util.List;
@Repository
public class CommentDao {
  @Autowired
  CommentRepository commentRepository;

  public List<Comment> findAll() {
    return commentRepository.findAll();
  }

  public Comment save(Comment comment) {
    return commentRepository.save(comment);
  }

  public Comment deleteById(Long id) {
     Comment comment = commentRepository.findById(id).orElse(null);
     commentRepository.deleteById(id);
     return comment;
  }
}
