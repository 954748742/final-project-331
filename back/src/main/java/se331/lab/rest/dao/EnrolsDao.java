package se331.lab.rest.dao;

import se331.lab.rest.entity.Enrols;

import java.util.List;

public interface EnrolsDao {
  List<Enrols> findAll();

  Enrols save(Enrols course);

  Enrols deleteById(long id);
}
