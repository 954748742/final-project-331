package se331.lab.rest.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Enrols;
import se331.lab.rest.repository.EnrolsRepository;

import java.util.List;

@Repository
public class EnrolsDaoImpl implements EnrolsDao{
  @Autowired
  EnrolsRepository enrolsRepository;
  @Override
  public List<Enrols> findAll() {
    return enrolsRepository.findAll();
  }

  @Override
  public Enrols save(Enrols enrols) {
    return enrolsRepository.save(enrols);
  }

  @Override
  public Enrols deleteById(long id) {
    Enrols enrols = enrolsRepository.findById(id).orElse(null);
    enrolsRepository.deleteById(id);
    return enrols;
  }
}
