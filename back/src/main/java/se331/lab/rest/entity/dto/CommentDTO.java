package se331.lab.rest.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import se331.lab.rest.entity.Comment;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentDTO {
  Long id;
  String name;
  String avatar;
  int  activity;
  String msg;
  String imgurl;
  String time;
  public static CommentDTO getCommentDTO(Comment comment) {
    return CommentDTO.builder()
      .id(comment.getId())
      .msg(comment.getMsg())
      .time(comment.getTime())
      .activity(comment.getActivity())
      .avatar(comment.getAvatar())
      .imgurl(comment.getImgurl())
      .name(comment.getName())
      .build();


  }

  public Comment getComment() {
    return Comment.builder()
      .id(this.id)
      .msg(this.msg)
      .time(this.time)
      .activity(this.activity)
      .imgurl(this.imgurl)
      .avatar(this.avatar)
      .name(this.name)
      .build();
  }
}
