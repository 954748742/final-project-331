package se331.lab.rest.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import se331.lab.rest.entity.Teacher;
import se331.lab.rest.security.entity.User;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TeacherDTO {
    Long id;
    String name;
    String surname;
    String imgurl;
    User user;
    public static TeacherDTO getTeacherDTO(Teacher teacher) {
        return TeacherDTO.builder()
                .id(teacher.getId())
                .name(teacher.getName())
                .surname(teacher.getSurname())
                .imgurl(teacher.getImgurl())
                .user(teacher.getUser())
                .build();
    }

    public Teacher getTeacher() {
        return Teacher.builder()
                .id(this.id)
                .name(this.name)
                .surname(this.surname)
                .imgurl(this.imgurl)
                .user(this.user)
                .build();
    }
}
