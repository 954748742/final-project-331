package se331.lab.rest.repository;

import org.springframework.data.repository.CrudRepository;
import se331.lab.rest.entity.Comment;

import java.util.List;

public interface CommentRepository extends CrudRepository<Comment,Long> {
  List<Comment> findAll();
}
