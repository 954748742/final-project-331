package se331.lab.rest.repository;

import org.springframework.data.repository.CrudRepository;
import se331.lab.rest.entity.Enrols;

import java.util.List;

public interface EnrolsRepository extends CrudRepository<Enrols,Long> {
  List<Enrols> findAll();
}
