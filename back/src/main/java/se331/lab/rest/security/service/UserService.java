package se331.lab.rest.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se331.lab.rest.dao.UserDao;
import se331.lab.rest.security.entity.User;

import java.util.List;

@Service
public class UserService {
  @Autowired
  UserDao userDao;

  @Transactional
  public List<User> getUsers() {
    return userDao.findAll();
  }
  @Transactional
  public User findById(Long id) {
    return userDao.findById(id);
  }

  @Transactional
  public User save(User user) {
    return userDao.save(user);
  }
}
