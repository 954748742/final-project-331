import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Activity } from '../entity/activity';
import { ActivityService } from '../services/activity.service';
import { Router } from '@angular/router';
import { TeacherService } from '../services/teacher.service';
import { Teacher } from '../entity/teacher';

@Component({
  selector: 'app-addactivity',
  templateUrl: './addactivity.component.html',
  styleUrls: ['./addactivity.component.css']
})

export class AddactivityComponent implements OnInit {
  teachers: Teacher[];
  fb: FormBuilder = new FormBuilder();
  AcForm: FormGroup;
  constructor(private activitySrv: ActivityService,  
              private router: Router,
              private teacherSrv: TeacherService) { 
      this.teacherSrv.obs().subscribe(teachers => this.teachers = teachers);
    
  }
   
  ngOnInit(): void {
    this.AcForm = this.fb.group({
      name: [null, Validators.required],
      desc: [null, Validators.required],
      loc: [null, Validators.required],
      regStart: [null, Validators.required],
      regEnd:[null, Validators.required],
      when: [null, Validators.required],
      teacher: [null, Validators.required],
      time: [null,Validators.required]
    }); 
  }

  validation_messages = {
    'name':[
      { type: 'required', message: 'activity name is required'},
    ],
    'desc': [
      { type: 'required', message: 'description is required' }
    ],
    'loc': [
      { type: 'required', message: 'the location is required' }
    ],
    'regStart': [
      { type: 'required', message: 'start date is required' }, 
    ],
    'regEnd': [
      { type: 'required', message: 'end date is required' }, 
    ],
    'when': [
      {type: 'required', message: 'activity date  is required' }
    ],
    'time': [
      {type: 'required', message: 'time  is required' }
    ],
    'teacher': [
      {type: 'required', message: 'teacher is required' }
    ],
  };

  addAc(){
    let fmVal = this.AcForm.value;
    console.log("form value: ", fmVal);
    let newAct: Activity = {
      id: 0,
      name: fmVal.name,
      desc: fmVal.desc,
      loc:  fmVal.loc,
      regStart: fmVal.regStart,
      regEnd: fmVal.regEnd,
      when: fmVal.when,
      teacher:  fmVal.teacher,
      time: fmVal.time
    }
    this.activitySrv.add(newAct);
    alert('activity created successfully');
    console.log(newAct);
  }
}
