import { MatDialogModule } from '@angular/material/dialog';
import { AppRoutingModule } from './core/app.routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, Input } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CustomMaterialModule } from './core/material.module';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { UserService } from './services/user.service';
import { UserFileImplService } from './services/user-file-impl.service';
import { UserApiImplService } from './services/user-api-impl.service';
import { StudentService } from './services/student.service';
import { StudentFileImplService } from './services/student-file-impl.service';
import { StudentApiImplService } from './services/student-api-impl.service';
import { TeacherService } from './services/teacher.service';
import { TeacherFileImplService } from './services/teacher-file-impl.service';
import { TeacherApiImplService } from './services/teacher-api-impl.service';
import { ActivityService } from './services/activity.service';
import { ActivityFileImplService } from './services/activity-file-impl.service';
import { ActivityApiImplService } from './services/activity-api-impl.service';
import { EnrolService } from './services/enrol.service';
import { EnrolFileImplService } from './services/enrol-file-impl.service';
import { EnrolApiImplService } from './services/enrol-api-impl.service';
import { MatTableModule, MatProgressSpinnerModule, MatInputModule, MatSortModule,
    MatPaginatorModule, MatMenuModule, MatButtonModule, MatIconModule, MatCardModule,
    MatSidenavModule, MatButtonToggleModule, MatListModule, MatOption, MatSelectModule,
    MatNativeDateModule, MatDatepickerModule, MatToolbarModule } from '@angular/material';
import { NavComponent } from './nav/nav.component';
import { UsersComponent } from './users/users.component';
import { RegisterComponent } from './register/register.component';
import { ChkstudentsComponent } from './chkstudents/chkstudents.component';
import { AddactivityComponent } from './addactivity/addactivity.component';
import { EnroledComponent } from './enroled/enroled.component';
import { ListactComponent } from './listact/listact.component';
import { WaitComponent } from './wait/wait.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { TlistactComponent } from './tlistact/tlistact.component';
import { ProfileComponent } from './profile/profile.component';
import { ImageviewComponent } from './imageview/imageview.component';
import { EditactComponent } from './editact/editact.component';
import { ListstuComponent } from './liststu/liststu.component';
import { HttpAuth } from './core/http.auth';
import { combineAll } from 'rxjs/operators';
import { CommentsComponent } from './comments/comments.component';
import { CommentService } from './services/comment.service';
import { CommentFileImplService } from './services/comment-file-impl.service';
import { CommentApiImplService } from './services/comment-api-impl.service';
import { TprofileComponent } from './tprofile/tprofile.component';
import { DelCommentsComponent } from './del-comments/del-comments.component';
import { MyNavComponent } from './my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        UsersComponent,
        NavComponent,
        RegisterComponent,
        ChkstudentsComponent,
        AddactivityComponent,
        EnroledComponent,
        ListactComponent,
        WaitComponent,
        TlistactComponent,
        ProfileComponent,
        ImageviewComponent,
        EditactComponent,
        ListstuComponent,
        CommentsComponent,
        TprofileComponent,
        DelCommentsComponent,
        MyNavComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        CustomMaterialModule,
        FormsModule,
        AppRoutingModule,
        HttpClientModule,
        MatTableModule,
        MatInputModule,
        MatPaginatorModule,
        MatSortModule,
        MatProgressSpinnerModule,
        MatMenuModule, 
        MatButtonModule, 
        MatIconModule, 
        MatCardModule, 
        MatSidenavModule,
        MatButtonToggleModule,
        MatListModule,
        FormsModule,
        MatSelectModule,
        MatNativeDateModule,
        MatDatepickerModule,
        NgxMaterialTimepickerModule.forRoot(),
        ReactiveFormsModule,
        MatDialogModule,
        LayoutModule,
        MatToolbarModule
    ],
    entryComponents: [ImageviewComponent, ImageviewComponent],
    providers: [
        { provide: UserService,         useClass: UserApiImplService },
        { provide: StudentService,      useClass: StudentApiImplService },
        { provide: TeacherService,      useClass: TeacherApiImplService },
        { provide: ActivityService,     useClass: ActivityApiImplService },
        { provide: EnrolService,        useClass: EnrolApiImplService },
        { provide: CommentService,      useClass: CommentApiImplService },
        { provide: HTTP_INTERCEPTORS,   useClass: HttpAuth, multi: true }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
