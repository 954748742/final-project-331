import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatDialog } from '@angular/material';

import { User } from '../entity/user';
import { Student } from '../entity/student';
import { StudentService } from '../services/student.service';
import { UserService } from '../services/user.service';
import { ImageviewComponent } from '../imageview/imageview.component';

class Row {
    id: number;
    email: string;
    pass: string;
    type: string;
    status: string;
    stid: string;
    name: string;
    surname: string;
    imgurl: string;
    dob: Date;
}

@Component({
    selector: 'app-chkstudents',
    templateUrl: './chkstudents.component.html',
    styleUrls: ['./chkstudents.component.css']
})
export class ChkstudentsComponent implements OnInit {
    students: Student[];
    users: User[];
    stuTable = new MatTableDataSource<Row>();
    @ViewChild(MatSort) sort: MatSort;
    columns: string[] = [
        'id', 'stid', 'name', 'surname', 'email', 'dob',
        'img', 'status', 'confirm', 'reject'
    ];

    constructor(private userSrv: UserService,
                private studentSrv: StudentService,
                private dialog: MatDialog) {
        this.studentSrv.obs().subscribe(students => {
            this.students = students;
            this.makeRows();
        });
        this.userSrv.obs().subscribe(users => {
            this.users = users;
            this.makeRows();
        });
    }

    makeRows() {
        if (this.users === null || this.students === null) return;
        let data: Row[] = new Array<Row>();
        for (let s of this.students) {
            let u = this.userSrv.get(s.id);
            let row: Row = {
                id: u.id,
                email: u.email,
                pass: "********",   // hidden
                type: u.type,
                status: u.status,
                stid: s.stid,
                name: s.name,
                surname: s.surname,
                imgurl: s.imgurl,
                dob: s.dob
            }
            data.push(row);
        }
        data = data.sort((a, b) => this.cmps(a.status, b.status));
        this.stuTable.data = data;
    }

    filterPred = (row: Row, filter: string) => {
        let instr = s => s.toLowerCase().indexOf(filter) != -1;
        let fields = [row.stid, row.name, row.surname, row.email, row.dob];
        for (let f of fields) { if (instr(f)) return true; }
        return false;
    }

    ngOnInit() {
        this.stuTable.sort = this.sort;
        this.stuTable.filterPredicate = this.filterPred;
    }

    cmps(a: string, b: string): number {
        if (a == b) return 0;
        if (a < b) return -1;
        else return 1;
    }

    cmpn(a: number, b: number): number {
        if (a == b) return 0;
        if (a < b) return -1;
        else return 1;
    }

    applyFilter(filter: string) {
        this.stuTable.filter = filter.trim().toLowerCase();
    }

    confirm(id: number) {
        let user = this.userSrv.get(id);
        user.status = "ok";
        if (user.type == "new-student") user.type = "student";
        this.userSrv.upd(user);
    }

    reject(id: number) {
        let user = this.userSrv.get(id);
        user.status = "reject";
        this.userSrv.upd(user);
    }

    openDialog(imageUrl: URL): void {
        const dialogRef = this.dialog.open(ImageviewComponent, {
            width: '50%',
            height: '90%',
            data: { image: imageUrl }
        });
    }

}
