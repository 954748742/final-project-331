import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../entity/user';
import { Activity } from '../entity/activity';
import { Comment } from '../entity/comment';
import { MatTableDataSource, MatSort } from '@angular/material';
import { IdService } from '../services/id.service';
import { ActivityService } from '../services/activity.service';
import { UserService } from '../services/user.service';
import { CommentService } from '../services/comment.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TeacherService } from '../services/teacher.service';
import { Teacher } from '../entity/teacher';
import { Student } from '../entity/student';
import { StudentService } from '../services/student.service';

class Row {
  id: number;
  name: string;
  imgurl: string;
  avatar: string;
  msg: string;
  time: string;
}

@Component({
  selector: 'app-del-comments',
  templateUrl: './del-comments.component.html',
  styleUrls: ['./del-comments.component.css']
})
export class DelCommentsComponent implements OnInit {
  msg: string;
  imgurl: string;
  id: User;
  activities: Activity[];
  users: User[];
  teachers: Teacher[];
  actName: string;
  students: Student[];
  comments: Comment[];
  comnt: Comment;
  comTable = new MatTableDataSource<Row>();
  @ViewChild(MatSort) sort: MatSort;
  columns: string[] = [
    'id', 'name', 'imgurl', 'avatar','msg', 'time', 'action'
  ];

  constructor(
    private idSrv: IdService,
    private activitySrv: ActivityService,
    private studentSrv: StudentService,
    private commentSrv: CommentService,
    private teacherSrv: TeacherService
  ) {
    this.idSrv.obsUser().subscribe(id => {
      this.id = id;
      this.makeRows();
    });
    this.activitySrv.obs().subscribe(activities => {
      this.activities = activities;
      this.makeRows();
    });
    this.teacherSrv.obs().subscribe(teachers => {
      this.teachers = teachers;
      this.makeRows();
    });
    this.studentSrv.obs().subscribe(students => {
      this.students = students;
      this.makeRows();
    });
    this.commentSrv.obs().subscribe(comments => {
      this.comments = comments;
      this.makeRows();
    });

  }

  makeRows() {
    if (!(this.activities && this.students && this.teachers && this.comments)) return;
    let data: Row[] = new Array<Row>();
    for (let c of this.comments) {
      let row: Row = {
        id:     c.id,
        name:   c.name,
        avatar: c.avatar,
        msg:    c.msg,
        imgurl: c.imgurl,
        time:   c.time,
      }
      data.push(row);
    }
    this.comTable.data = data;
  }

  delcomment(id: number) {
    this.commentSrv.del(id);
  }

  ngOnInit() {
    this.comTable.sort = this.sort;
  }

}
