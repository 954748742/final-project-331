import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { Activity } from '../entity/activity';
import { Teacher } from '../entity/teacher';
import { ActivityService } from '../services/activity.service';
import { TeacherService } from '../services/teacher.service';

@Component({
    selector: 'app-editact',
    templateUrl: './editact.component.html',
    styleUrls: ['./editact.component.css']
})
export class EditactComponent implements OnInit {
    teachers: Teacher[];
    activities: Activity[];
    actId: number;
    form: FormGroup;

    constructor(private router: Router,
                private route: ActivatedRoute,
                private activitySrv: ActivityService,
                private teacherSrv: TeacherService,
                private fb: FormBuilder) {
        this.form = this.fb.group({
            name:       ["", [ Validators.required ]],
            desc:       ["", [ Validators.required ]],
            loc:        ["", [ Validators.required ]],
            when:       ["", [ Validators.required ]],
            time:       ["", [ Validators.required ]],
            teacher:    ["", [ Validators.required ]],
            regStart:   ["", [ Validators.required ]],
            regEnd:     ["", [ Validators.required ]],
        });
        this.route.params.subscribe(params => {
            this.actId = params['id'];
            this.loadAct();
        });
        this.activitySrv.obs().subscribe(activities => {
            this.activities = activities;
            this.loadAct();
        });
        this.teacherSrv.obs().subscribe(teachers => {
            this.teachers = teachers;
            this.loadAct();
        });
    }

    loadAct() {
        if (!(this.activities && this.teachers && this.actId)) return;
        let act = this.activitySrv.get(this.actId);
        this.form.setValue({
            name:       act.name,
            desc:       act.desc,
            loc:        act.loc,
            when:       act.when,
            time:       act.time,
            teacher:    act.teacher,
            regStart:   act.regStart,
            regEnd:     act.regEnd,
        });
    }

    get name()      { return this.form.get('name'); }
    get desc()      { return this.form.get('desc'); }
    get loc()       { return this.form.get('loc'); }
    get when()      { return this.form.get('when'); }
    get time()      { return this.form.get('time'); }
    get teacher()   { return this.form.get('teacher'); }
    get regStart()  { return this.form.get('regStart'); }
    get regEnd()    { return this.form.get('regEnd'); }

    errs = {
        'name':     [ {type: 'required', msg: 'an activity name is required'} ],
        'desc':     [ {type: 'required', msg: 'a description is required'} ],
        'loc':      [ {type: 'required', msg: 'the location is required'} ],
        'when':     [ {type: 'required', msg: 'activity date is required'} ],
        'time':     [ {type: 'required', msg: 'activity time is required'} ],
        'teacher':  [ {type: 'required', msg: 'a teacher is required'} ],
        'regStart': [ {type: 'required', msg: 'registration start date is required'} ],
        'regEnd':   [ {type: 'required', msg: 'registration end date is required'} ],
    };

    updAct() {
        let fmVal = this.form.value;
        let act: Activity = {
            id:         this.actId,
            name:       fmVal.name,
            desc:       fmVal.desc,
            loc:        fmVal.loc,
            regStart:   fmVal.regStart,
            regEnd:     fmVal.regEnd,
            when:       fmVal.when,
            time:       fmVal.time,
            teacher:    fmVal.teacher,
        }
        if (this.activitySrv.upd(act)) {
            this.router.navigate(["/tlistact"]);
        } else {
            alert("Update could not be saved");
        }
    }

    ngOnInit() { }

}
