import { User } from '../entity/user';
import { Activity } from '../entity/activity';
import { Teacher } from '../entity/teacher';
import { Enrol } from '../entity/enrol';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IdService } from '../services/id.service';
import { ActivityService } from '../services/activity.service';
import { TeacherService } from '../services/teacher.service';
import { EnrolService } from '../services/enrol.service';
import { MatTableDataSource, MatSort } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';

class Row {
    id: number;
    name: string;
    loc: string;
    desc: string;
    when: string;
    time: string;
    teacher: string;
    status: string;
}

@Component({
    selector: 'app-enroled',
    templateUrl: './enroled.component.html',
    styleUrls: ['./enroled.component.css']
})
export class EnroledComponent implements OnInit {
    id: User;
    activities: Activity[];
    teachers: Teacher[];
    enrols: Enrol[];
    enroledTable = new MatTableDataSource<Row>();
    @ViewChild(MatSort) sort: MatSort;
    columns: string[] = [
        'id', 'name', 'loc', 'when', 'time', 'teacher', 'status', 'drop'
    ];

    constructor(private idSrv: IdService,
                private activitySrv: ActivityService,
                private teacherSrv: TeacherService,
                private enrolSrv: EnrolService
                ) {
        this.idSrv.obsUser().subscribe(id => {
            this.id = id;
            this.makeRows();
        });
        this.activitySrv.obs().subscribe(activities => {
            this.activities = activities;
            this.makeRows();
        });
        this.teacherSrv.obs().subscribe(teachers => {
            this.teachers = teachers;
            this.makeRows();
        });
        this.enrolSrv.obs().subscribe(enrols => {
            this.enrols = enrols;
            this.makeRows();
        });
    }

    makeRows() {
        if (this.id == null || this.activities == null
            || this.teachers == null || this.enrols == null) return;
        let sid: number = this.id.id;
        let data: Row[] = new Array<Row>();
        for (let e of this.enrols) {
            if (e.student != sid) continue;
            let act = this.activitySrv.get(e.activity);
            let t = this.teacherSrv.get(act.teacher);
            let row: Row = {
                id: e.id,
                name: act.name,
                loc: act.loc,
                desc: act.desc,
                when: act.when,
                time: act.time,
                teacher: t.name,
                status: e.status
            }
            data.push(row);
        }
        this.enroledTable.data = data;
    }

    ngOnInit() {
        this.enroledTable.sort = this.sort;
    }

    updateFilter(filter: string) {
        this.enroledTable.filter = filter;
    }

    drop(enrolid: number) {
        this.enrolSrv.del(enrolid);
    }

}
