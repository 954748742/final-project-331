export class Activity {
    id: number;
    name: string;
    loc: string;
    desc: string;
    when: string;
    time: string;
    teacher: number;
    regStart: string;
    regEnd: string;
}
