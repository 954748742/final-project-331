export class Student {
    id: number;
    stid: string;
    name: string;
    surname: string;
    imgurl: string;
    dob: Date;
}
