export class User {
    id: number;
    email: string;
    pass: string;
    type: string;
    status: string;
}
