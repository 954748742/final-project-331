import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-imageview',
  templateUrl: './imageview.component.html',
  styleUrls: ['./imageview.component.css']
})
export class ImageviewComponent implements OnInit {
  imageUrl: URL;

  constructor(
      public dialogRef: MatDialogRef<ImageviewComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any) {
      this.imageUrl = data.image;
  }

  ngOnInit() { }

}

