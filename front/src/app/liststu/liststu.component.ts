import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MatTableDataSource, MatSort, MatDialog } from '@angular/material';

import { User } from '../entity/user';
import { UserService } from '../services/user.service';
import { Student } from '../entity/student';
import { StudentService } from '../services/student.service';
import { Activity } from '../entity/activity';
import { ActivityService } from '../services/activity.service';
import { Enrol } from '../entity/enrol';
import { EnrolService } from '../services/enrol.service';
import { ImageviewComponent } from '../imageview/imageview.component';

class Row {
    id: number;
    stid: string;
    name: string;
    surname: string;
    email: string;
    dob: Date;
    imgurl: string;
    status: string;
}

@Component({
  selector: 'app-liststu',
  templateUrl: './liststu.component.html',
  styleUrls: ['./liststu.component.css']
})
export class ListstuComponent implements OnInit {
    users: User[];
    students: Student[];
    activities: Activity[];
    enrols: Enrol[];
    actId: number;
    actName: string;
    stuTable = new MatTableDataSource<Row>();
    @ViewChild(MatSort) sort: MatSort;
    columns: string[] = [
        'id', 'stid', 'name', 'surname', 'email', 'dob',
        'img', 'status', 'confirm', 'reject'
    ];

    constructor(private router: Router,
                private route: ActivatedRoute,
                private userSrv: UserService,
                private studentSrv: StudentService,
                private activitySrv: ActivityService,
                private enrolSrv: EnrolService,
                private dialog: MatDialog) {
        this.route.params.subscribe(params => {
            this.actId = params['id'];
            this.makeRows();
            if (this.activities) {
                this.actName = this.activitySrv.get(this.actId).name;
            }
        });
        this.userSrv.obs().subscribe(users => {
            this.users = users;
            this.makeRows();
        });
        this.studentSrv.obs().subscribe(students => {
            this.students = students;
            this.makeRows();
        });
        this.activitySrv.obs().subscribe(activities => {
            this.activities = activities;
            if (this.actId) this.actName = this.activitySrv.get(this.actId).name;
        });
        this.enrolSrv.obs().subscribe(enrols => {
            this.enrols = enrols;
            this.makeRows();
        });
    }

    makeRows() {
        if (!(this.actId && this.users && this.students && this.enrols)) return;
        let data: Row[] = new Array<Row>();
        for (let s of this.students) {
            let u = this.userSrv.get(s.id);
            let en = this.enrolSrv.getPair(s.id, this.actId);
            if (!en) continue;
            let row: Row = {
                id: u.id,
                stid: s.stid,
                name: s.name,
                surname: s.surname,
                email: u.email,
                dob: s.dob,
                imgurl: s.imgurl,
                status: en.status,
            }
            data.push(row);
        }
        data = data.sort((a, b) => this.cmps(a.status, b.status));
        this.stuTable.data = data;
    }

    filterPred = (row: Row, filter: string) => {
        let instr = s => s.toLowerCase().indexOf(filter) != -1;
        let fields = [row.stid, row.name, row.surname, row.email, row.dob];
        for (let f of fields) { if (instr(f)) return true; }
        return false;
    }

    ngOnInit() {
        this.stuTable.sort = this.sort;
        this.stuTable.filterPredicate = this.filterPred;
    }

    cmps(a: string, b: string): number {
        if (a == b) return 0;
        if (a < b) return -1;
        else return 1;
    }

    cmpn(a: number, b: number): number {
        if (a == b) return 0;
        if (a < b) return -1;
        else return 1;
    }

    applyFilter(filter: string) {
        this.stuTable.filter = filter.trim().toLowerCase();
    }

    confirm(sid: number) {
        let enrol = this.enrolSrv.getPair(sid, this.actId);
        enrol.status = "confirmed";
        this.enrolSrv.upd(enrol);
    }

    reject(sid: number) {
        let enrol = this.enrolSrv.getPair(sid, this.actId);
        enrol.status = "rejected";
        this.enrolSrv.upd(enrol);
    }

    openDialog(imageUrl: URL): void {
        const dialogRef = this.dialog.open(ImageviewComponent, {
            width: '50%',
            height: '90%',
            data: { image: imageUrl }
        });
    }

}
