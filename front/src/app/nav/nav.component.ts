import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatMenuModule } from '@angular/material/menu';
import { IdService } from './../services/id.service';
import { User } from '../entity/user';
import { Student } from '../entity/student';
import { StudentService } from '../services/student.service';
import { Teacher } from '../entity/teacher';
import { TeacherService } from '../services/teacher.service';

@Component({
    selector: 'app-nav',
    templateUrl: './nav.component.html',
    styleUrls: ['./nav.component.css']
})
export class NavComponent {
    login = false;
    user: User;
    students: Student[];
    teachers: Teacher[];
    leftItems:  Object[] = [];
    rightItems: Object[] = [];
    name = "";
    avatar = "";

    constructor(private id: IdService,
                private studentSrv: StudentService,
                private teacherSrv: TeacherService,
                private router: Router) {
        this.id.obsUser().subscribe((user) => {
            this.setUser(user);
        });
        this.studentSrv.obs().subscribe(students => {
            this.students = students;
            this.setUser(this.user);
        });
        this.teacherSrv.obs().subscribe(teachers => {
            this.teachers = teachers;
            this.setUser(this.user);
        });
    }

    setUser(user: User) {
        this.user = user;
        if (user == null) {
            this.login = false;
            this.setType(null);
            this.router.navigate(["/login"]);
        } else {
            if (this.students == null || this.teachers == null) return;
            this.login = true;
            this.setType(user.type);
        }
    }

    logout() {
        this.id.logout();
    }

    setType(type: string) {
        let logoutfn = () => { this.logout(); }
        switch (type) {
        case "admin": {
            this.name = "Admin";
            this.avatar = "";
            this.leftItems = [
                { route: ['/users'],        label: "List users" },
                { route: ['/chkstudents'],  label: "Check students" },
                { route: ['/addact'],       label: "Add activity" },
                { route: ['/delcomments'],  label: "Comments" },
            ];
            this.rightItems = [
                { call:  logoutfn,          label: "Logout" }
            ];
            break;
        }
        case "student": {
            let s = this.studentSrv.get(this.user.id);
            this.name = `Student: ${s.name}`;
            this.avatar = s.imgurl;
            this.leftItems = [
                { route: ['/listact'],  label: "Activities" },
                { route: ['/enroled'],  label: "Enroled" },
            ];
            this.rightItems = [
                { route: ['/profile'],  label: "Edit your profile" },
                { call:  logoutfn,      label: "Logout" },
            ];
            break;
        }
        case "new-student": {
            let s = this.studentSrv.get(this.user.id);
            this.name = `New student: ${s.name}`;
            this.avatar = s.imgurl;
            this.leftItems = [
            ];
            this.rightItems = [
                { route: ['/profile'],  label: "Edit your profile" },
                { call:  logoutfn,      label: "Logout" },
            ];
            break;
        }
        case "teacher": {
            let t = this.teacherSrv.get(this.user.id);
            this.name = `Teacher: ${t.name}`;
            this.avatar = t.imgurl;
            this.leftItems = [
                { route: ['/tlistact'],  label: "Activities" },
            ];
            this.rightItems = [
                { route: ['/tprofile'],  label: "Edit your profile" },
                { call:  logoutfn,       label: "Logout" },
            ];
            break;
        }
        default: {
            this.name = "";
            this.avatar = "";
            this.leftItems = [
                { route: ['/login'],    label: "Login" },
                { route: ['/register'], label: "Register" },
            ];
            this.rightItems = [];
        }
        }
    }

}

