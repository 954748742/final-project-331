import { TestBed, inject } from '@angular/core/testing';

import { ActivityApiImplService } from './activity-api-impl.service';

describe('ActivityApiImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActivityApiImplService]
    });
  });

  it('should be created', inject([ActivityApiImplService], (service: ActivityApiImplService) => {
    expect(service).toBeTruthy();
  }));
});
