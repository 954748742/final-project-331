import { TestBed, inject } from '@angular/core/testing';

import { ActivityFileImplService } from './activity-file-impl.service';

describe('ActivityFileImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActivityFileImplService]
    });
  });

  it('should be created', inject([ActivityFileImplService], (service: ActivityFileImplService) => {
    expect(service).toBeTruthy();
  }));
});
