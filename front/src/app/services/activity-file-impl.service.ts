import { Activity } from '../entity/activity';
import { ActivityService } from './activity.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ActivityFileImplService extends ActivityService {
    private activities = new BehaviorSubject<Activity[]>(null);

    constructor(private http: HttpClient) {
        super();
        this.http.get<Activity[]>('assets/activities.json').subscribe(activities => {
            this.activities.next(activities);
        });
    }

    load() { }

    obs(): Observable<Activity[]> {
        return this.activities.asObservable();
    }

    get(id: number): Activity {
        if (this.activities == null) return null;
        let list = this.activities.getValue();
        return list.find(obj => obj.id == id);
    }

    private nextId(): number {
        let list = this.activities.getValue();
        return list.map(obj => obj.id).reduce((a,b) => a > b ? a : b) + 1;
    }

    add(activity: Activity): Activity {
        if (this.activities == null) return null;
        let list = this.activities.getValue();
        activity.id = this.nextId();
        list.push(activity);
        this.activities.next(list);
        return activity;
    }

    del(id: number): Activity {
        if (this.activities == null) return null;
        let list = this.activities.getValue();
        let activity = list.find(obj => obj.id == id);
        if (activity == null) return null;
        let i = list.indexOf(activity);
        list.splice(i, 1);
        this.activities.next(list);
        return activity;
    }

    upd(activity: Activity): Activity {
        if (this.activities == null) return null;
        let list = this.activities.getValue();
        let old = list.find(obj => obj.id == activity.id);
        if (old == null) return null;
        let i = list.indexOf(old);
        list[i] = activity;
        this.activities.next(list);
        return old;
    }

}

