import { Injectable } from '@angular/core';
import { Comment } from '../entity/comment';
import { CommentService } from './comment.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommentApiImplService extends CommentService {
    private comments = new BehaviorSubject<Comment[]>(null);
    private api = environment.commentApi;

    constructor(private http: HttpClient) { 
        super();
        this.load();
    }

    load() {
        this.http.get<Comment[]>(this.api)
        .subscribe(comments => {
            this.comments.next(comments);
        });
    }

    obs(): Observable<Comment[]> {
        return this.comments.asObservable();
    }

    get(id: number): Comment {
        if (this.comments == null) return null;
        let list = this.comments.getValue();
        return list.find(obj => obj.id == id);
    }

    private nextId(): number {
        let list = this.comments.getValue();
        return list.map(obj => obj.id).reduce((a,b) => a > b ? a : b) + 1;
    }

    add(comment: Comment): Comment {
        if (this.comments == null) return null;
        let list = this.comments.getValue();
        comment.id = this.nextId();
        list.push(comment);
        this.comments.next(list);
        this.http.post<any>(this.api, Comment)
            .subscribe(data => { this.load(); });
        return comment;
    }

    del(id: number): Comment {
        if (this.comments == null) return null;
        let list = this.comments.getValue();
        let comment = list.find(obj => obj.id == id);
        if (comment == null) return null;
        let i = list.indexOf(comment);
        list.splice(i, 1);
        this.comments.next(list);
        this.http.delete<any>(this.api + "/" + id)
            .subscribe(data => { this.load(); });
        return comment;
    }

    upd(comment: Comment): Comment {
        if (this.comments == null) return null;
        let list = this.comments.getValue();
        let old = list.find((obj) => obj.id === comment.id);
        if (old === null) return null;
        let i = list.indexOf(old);
        list[i] = comment;
        this.comments.next(list);
        this.http.put<any>(this.api + "/" + comment.id, comment)
            .subscribe(data => { this.load(); });
        return old;
    }

}
