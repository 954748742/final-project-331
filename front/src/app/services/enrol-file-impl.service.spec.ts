import { TestBed, inject } from '@angular/core/testing';

import { EnrolFileImplService } from './enrol-file-impl.service';

describe('EnrolFileImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EnrolFileImplService]
    });
  });

  it('should be created', inject([EnrolFileImplService], (service: EnrolFileImplService) => {
    expect(service).toBeTruthy();
  }));
});
