import { Observable } from 'rxjs';
import { Enrol } from '../entity/enrol';

export abstract class EnrolService {
    abstract load();
    abstract obs(): Observable<Enrol[]>;
    abstract get(id: number): Enrol;
    abstract getPair(stu: number, act: number): Enrol;
    abstract add(enrol: Enrol): Enrol;
    abstract del(id: number): Enrol;
    abstract upd(enrol: Enrol): Enrol;
}

