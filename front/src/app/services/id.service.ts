import { User } from '../entity/user';
import { UserService } from './user.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';
import { map, filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class IdService {
    user = new BehaviorSubject<User>(null);

    constructor(private http: HttpClient) { }

    login(uname: string, pass: string): Observable<User> {
        this.http.post<any>(environment.authApi,
            { username: uname, password: pass })
            .subscribe(data => {
                this.setupUser(data);
            });
        // filter takes a fn of u=user obj and n=counting number from 0
        // let notFirstNull = filter((u: User, n) => n > 0 || u != null);
        // notFirstNull will filter out the first user if it is null
        // return notFirstNull(this.user.asObservable());
        return this.user.asObservable();
    }

    setupUser(d) {
        if (d.token && d.user) {
            localStorage.setItem('token', d.token);
            let newUser: User = <User>d.user;
            this.user.next(newUser);
        } else {
            console.log("Bad auth response: ", d);
            this.user.next(null);
        }
    }

    logout() {
        localStorage.removeItem('token');
        this.user.next(null);
    }

    obsUser(): Observable<User> {
        return this.user.asObservable();
    }

    getUser(): User {
        return this.user.getValue();
    }

}
