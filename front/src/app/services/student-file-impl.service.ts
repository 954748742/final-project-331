import { Student } from '../entity/student';
import { StudentService } from './student.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudentFileImplService extends StudentService {
    private students = new BehaviorSubject<Student[]>(null);

    constructor(private http: HttpClient) {
        super();
        this.http.get<Student[]>('assets/students.json').subscribe(
        students => {
            for (let s of students) this.anonPic(s);
            this.students.next(students);
        });
    }

    load() { }

    obs(): Observable<Student[]> {
        return this.students.asObservable();
    }

    get(id: number): Student {
        if (this.students == null) return null;
        let list = this.students.getValue();
        return list.find((obj) => obj.id === id);
    }

    private nextId(): number {
        let list = this.students.getValue();
        return list.map(obj => obj.id).reduce((a,b) => a > b ? a : b) + 1;
    }

    add(student: Student): Student {
        if (this.students == null) return null;
        let list = this.students.getValue();
        this.anonPic(student);
        list.push(student);
        this.students.next(list);
        return student;
    }

    del(id: number): Student {
        if (this.students == null) return null;
        let list = this.students.getValue();
        let student = list.find((obj) => obj.id === id);
        if (student === null) return null;
        let i = list.indexOf(student);
        list.splice(i, 1);
        this.students.next(list);
        return student;
    }

    upd(student: Student): Student {
        if (this.students == null) return null;
        let list = this.students.getValue();
        let old = list.find((obj) => obj.id === student.id);
        if (old === null) return null;
        this.anonPic(student);
        let i = list.indexOf(old);
        list[i] = student;
        this.students.next(list);
        return old;
    }

    anonPic(student: Student): Student {
        if (student.imgurl == null || student.imgurl == "") {
            student.imgurl = "assets/anon.png";
        }
        return student;
    }
}

