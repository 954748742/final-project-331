import { TestBed, inject } from '@angular/core/testing';

import { UserApiImplService } from './user-api-impl.service';

describe('UserApiImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserApiImplService]
    });
  });

  it('should be created', inject([UserApiImplService], (service: UserApiImplService) => {
    expect(service).toBeTruthy();
  }));
});
