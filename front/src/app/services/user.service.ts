import { Observable } from 'rxjs';
import { User } from '../entity/user';

export abstract class UserService {
    abstract load();
    abstract obs(): Observable<User[]>;
    abstract get(id: number): User;
    abstract add(user: User): User;
    abstract del(id: number): User;
    abstract upd(user: User): User;
}
