import { Activity } from './../entity/activity';
import { User } from '../entity/user';
import { Teacher } from '../entity/teacher';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IdService } from '../services/id.service';
import { ActivityService } from '../services/activity.service';
import { TeacherService } from '../services/teacher.service';
import { MatTableDataSource, MatSort, MatInput } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';

class Row {
    id: number;
    name: string;
    loc: string;
    desc: string;
    regStart: string;
    regEnd: string;
    when: string;
    time: string;
}

@Component({
    selector: 'app-tlistact',
    templateUrl: './tlistact.component.html',
    styleUrls: ['./tlistact.component.css']
})
export class TlistactComponent implements OnInit {
    id: User;
    activities: Activity[];
    teachers: Teacher[];
    actTable = new MatTableDataSource<Row>();
    @ViewChild(MatSort) sort: MatSort;
    columns: string[] = [
        'id', 'name', 'loc', 'desc', 'regStart', 'regEnd',
        'when', 'time', 'comment', 'edit'
    ];
    filter: string = "";
    minDate: string = "1970-01-01";
    maxDate: string = "9999-01-01";

    fb: FormBuilder = new FormBuilder();
    filterForm: FormGroup = this.fb.group({
        minDate: [null],
        maxDate: [null],
        filter: [null]
    })

    constructor(private idSrv: IdService,
        private activitySrv: ActivityService,
        private teacherSrv: TeacherService
    ) {
        this.idSrv.obsUser().subscribe(id => {
            this.id = id;
            this.makeRows();
        });
        this.activitySrv.obs().subscribe(activities => {
            this.activities = activities;
            this.makeRows();
        });
        this.teacherSrv.obs().subscribe(teachers => {
            this.teachers = teachers;
            this.makeRows();
        });

    }

    makeRows() {
        if (this.id == null || this.activities == null
            || this.teachers == null
        ) return;
        let sid: number = this.id.id;
        let today = new Date();
        let data: Row[] = new Array<Row>();
        for (let act of this.activities) {
            let t = this.teacherSrv.get(this.id.id);
            let row: Row = {
                id: act.id,
                name: act.name,
                loc: act.loc,
                desc: act.desc,
                regStart: act.regStart,
                regEnd: act.regEnd,
                when: act.when,
                time: act.time,
            }
            if(t == undefined) return;
            if (act.teacher == t.id) data.push(row);

        }
        this.actTable.data = data;
    }

    ngOnInit() {
        this.actTable.sort = this.sort;
        this.actTable.filterPredicate = this.dateFilter;
        this.actTable.filter = '.';
    }

    dateFilter = (row: Row, filter: string) => {
        let accept: boolean = row.name.indexOf(filter.substring(1)) > -1;
        if (this.minDate != null) accept = accept && row.when >= this.minDate;
        if (this.maxDate != null) accept = accept && row.when <= this.maxDate;
        return accept;
    }

    updateFilter() {
        let fmVal = this.filterForm.value;
        let filter = fmVal.filter == null ? "" : fmVal.filter.trim().toLowerCase();
        this.minDate = fmVal.minDate;
        this.maxDate = fmVal.maxDate;
        this.actTable.filter = "." + filter;
        this.makeRows();
    }

    applyFilter(filter: string) {
        this.actTable.filter = filter.trim().toLowerCase();
    }
    
    resetForm() {
        this.filterForm.value.minDate = " ";
        this.filterForm.value.maxDate = " ";
    };

}
