import { Teacher } from './../entity/teacher';
import { Component, OnInit, ViewChild } from "@angular/core";
import { Student } from "../entity/student";
import { User } from "../entity/user";
import { MatTableDataSource, MatSort } from "@angular/material";
import { constructor, reject } from "q";
import { UserService } from "../services/user.service";
import { StudentService } from "../services/student.service";
import { a, b } from "@angular/core/src/render3";
import { TeacherService } from "../services/teacher.service";

class Row {
    id: number;
    name: string;
    email: string;
    pass: string;
    type: string;
    status: string;
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
   
    students: Student[];
    users: User[];
    teachers: Teacher[];
    dataSource = new MatTableDataSource<Row>();
    data: Row[] = new Array<Row>();
    @ViewChild(MatSort) sort: MatSort;
    columns: string[] = [
        'id', 'name','email', 'type', 'status'
    ];

    constructor(private userSrv: UserService,
                private studentSrv: StudentService,
                private teacherSrv: TeacherService) {
        this.studentSrv.obs().subscribe(students => {
            this.students = students;
            this.makeRows();
        });
        this.userSrv.obs().subscribe(users => {
            this.users = users;
            this.makeRows();
        });
        this.teacherSrv.obs().subscribe(teachers => {
            this.teachers = teachers;
            this.makeRows();
        });
    }

    makeRows() {
        if (this.users == null || this.teachers == null || this.students == null) return;
        for (let u of this.users) {
            let name = u.type;
            if (u.type == "teacher") {
                let t = this.teacherSrv.get(u.id);
                name = t.name;
            } else if (u.type == "student" || u.type == "new-student") {
                let s = this.studentSrv.get(u.id);
                name = s.name + ' ' + s.surname;
            }
            let row: Row = {
                id: u.id,
                name: name,
                email: u.email,
                pass: '********',
                type: u.type,
                status: u.status
            }
            this.data.push(row);
        } 
        this.data = this.data.sort((a,b) => this.cmps(a.status, b.status));
        this.dataSource.data = this.data;
    }

    ngOnInit() { this.dataSource.sort = this.sort; }

    cmps(a: string, b: string): number {
        if (a == b) return 0;
        if (a < b) return -1;
        else return 1;
    }

    cmpn(a: number, b: number): number {
        if (a == b) return 0;
        if (a < b) return -1;
        else return 1;
    }

    applyFilter(filter: string) {
        this.dataSource.filter = filter.trim().toLowerCase();
    }

}


